/* See LICENSE file for copyright and license details. */

#include <complex.h>
#include <stdio.h>

#include "impedance.h"

#include "config.h"

/* Functions implementations */
int
impedance(int period, float *vi, float *vo, float *frequency, float *x)
{
	int i;

	float complex fftin[N/2], fftout[N/2];

	int maxpos;
	float dv;

	fft_calculate(vi, fftin);
	fft_calculate(vo, fftout);

	maxpos = 1;
	for (i=1; i<N/2; i++)
		if (cabs(fftin[maxpos])<cabs(fftin[i]))
			maxpos = i;

	/* Check differential voltage */
	dv = carg(fftout[maxpos]/fftin[maxpos])*180/PI;
	if (dv<0)
		dv = -dv;
	if ((dv<10)||(80<dv))
		return 2;

	/* Calculate frequency */
	*frequency = (1/(period*1e-3*10))*maxpos;
	*x = cimag(resistor*((fftin[maxpos]/fftout[maxpos])-1));

	return 0;
}
