/* See LICENSE file for copyright and license details. */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

char*argv0;
#include "serial.h"
#include "impedance.h"

#include "arg.h"
#include "util.h"

#include "config.h"

/* Functions declarations */
static int getdata(int period, float *frequency, float *x);
static void measureimpedance(void);
static void linearregression(int n, float *xi, float *yi,
		float *alpha, float *beta);

static void usage(void);

/* Globals */
static char *serialdevice = NULL;
static int serialbaud = 0;

/* Functions implementations */
int
getdata(int period, float *frequency, float *x)
{
	int i, j;
	char c;
	char mess[8];

	int sampnum[N], sampin[N], sampout[N];

	float sampinfft[N], sampoutfft[N];

	/* Clear data*/
	for (i=0; i<N; i++)
		sampnum[i] = sampin[i] = sampout[i] = 0;
	/* Clear serial port */
	serial_clean();

	/* Send period */
	sprintf(mess, "(%04d)", period);
	while (serial_writechar(mess, sizeof(mess)));

	/* Receive data*/
	for (i=0; i<N; )
	{
		/* Start data */
		c = '\0';
		while (serial_readchar(&c, sizeof(c)));
		if (c!='(')
			continue;

		/* Num */
		memset(mess, 0, sizeof(mess));
		for (j=0; j<4; j++)
		{
			while (serial_readchar(&c, sizeof(c)));
			mess[j] = c;
		}
		while (serial_readchar(&c, sizeof(c)));
		if (c!=',')
			continue;
		sampnum[i] = atoi(mess);

		/* Data in */
		memset(mess, 0, sizeof(mess));
		for (j=0; j<5; j++)
		{
			while (serial_readchar(&c, sizeof(c)));
			mess[j] = c;
		}
		while (serial_readchar(&c, sizeof(c)));
		if (c!=',')
			continue;
		sampin[i] = atoi(mess);

		/* Data out */
		memset(mess, 0, sizeof(mess));
		for (j=0; j<5; j++)
		{
			while (serial_readchar(&c, sizeof(c)));
			mess[j] = c;
		}
		while (serial_readchar(&c, sizeof(c)));

		sampout[i] = atoi(mess);

		/* End data */
		if (c!=')')
			continue;

		/* Next data */
		i++;
	}

	/* Check data */
	for (i=0; i<N; i++)
		if (	(i!=sampnum[i])||
			(sampin[i]<0)||(65535<sampin[i])||
			(sampout[i]<0)||(65535<sampout[i])
			)
			return 1;

	/* Copy data */
	for (i=0; i<N; i++)
	{
		sampinfft[i] = sampin[i]*3.3/65535;
		sampoutfft[i] = sampout[i]*3.3/65535;
	}

	/* Get frequency */
	if (impedance(period, sampinfft, sampoutfft, frequency, x))
		return 2;
	return 0;
}

void
measureimpedance()
{
	int i;
	char c;

	float base;

	int samp;
	float freq, impedance, reac;

	int ns, fc;
	float xt[10], ft[10], alpha, beta;

	/* Clear serial port */
	while (!serial_readchar(&c, sizeof(c)));

	base = pow(500, 1/10.0);
	ns = 0;

	/* Test low resolution */
	printf("test frequency ");

	for (i=1; i<=10; i++)
	{
		fflush(stdout);
		samp = (int)pow(base, i);
		if (500<samp)
			samp = 500;
		if (getdata(samp, &freq, &impedance))
		{
			printf(".");
			continue;
		}

		printf("*");
		if (impedance<0)
			impedance = -impedance;
		ft[ns] = log(freq);
		xt[ns] = log(impedance);
		ns++;
	}
	printf("\n");

	/* Check if exist valid frequency */
	if (ns<2)
	{
		die("no impendance.\n");
	}

	/* Linear regression */
	linearregression(ns, ft, xt, &alpha, &beta);

	/* Frequency to maximum power transfer */
	fc = 1e3/exp((log(resistor)-alpha)/beta);

	if ((fc<1)||(500<fc))
		die("no found frequency to maximum power transfer.\n");

	if (getdata(fc, &freq, &impedance))
		die("error in use frequency to maximum power transfer.\n");
	printf("frequency: %.3f Hz\nreactance: %.0f Ohm\n", freq, impedance);

	if (impedance<0)
		reac = -1/(2*PI*impedance*freq);
	else
		reac = 2*PI*impedance*freq;

	/* Units */
	c = ' ';
	if ((1e-3<=reac)&&(reac<1))
	{
		c = 'm';
		base = 1e3;
	}
	if ((1e-6<=reac)&&(reac<1e-3))
	{
		c = 'u';
		base = 1e6;
	}
	if ((1e-9<=reac)&&(reac<1e-6))
	{
		c = 'n';
		base = 1e9;
	}

	if (impedance<0)
		printf("capacitance: %.3f %cF\n", base*reac, c);
	else
		printf("inductance: %.3f %cH\n", base*reac, c);
}


void
linearregression(int n, float *xi, float *yi, float *alpha, float *beta)
{
	int i;

	float y, x, x2, xy;

	y = x = x2 = xy = 0.0;

	for (i=0; i<n; i++)
	{
		y += yi[i];
		x += xi[i];
		x2 += xi[i]*xi[i];
		xy += xi[i]*yi[i];
	}

	*beta = (n*xy-x*y)/(n*x2 - x*x);
	*alpha = (y-*beta*x)/n;
}
void
usage()
{
	die(	"usage: smr [OPTION]\n"
		"  -b        Baudrate\n"
		"  -d        Device\n"
		"  -v        Show version\n"
		"  -h        Help command\n" );
}

int
main(int argc, char *argv[])
{
	/* Arguments */
	ARGBEGIN {
	case 'b':
		serialbaud = atoi(EARGF(usage()));
		break;
	case 'd':
		serialdevice = EARGF(usage());
		break;
	case 'v':
		die("smr-"VERSION"\n");
		return 0;
	default:
		usage();
	} ARGEND

	/* Serial port configuration */
	if (!serialbaud)
		serialbaud = 9600;

	if (!serialdevice)
		die("no device\n");

	/* Open port */
	if (serial_openport(serialdevice, serialbaud))
		die("%s no connect\n", serialdevice);

	/* Measure impedance */
	measureimpedance();

	/* Close */
	serial_closeport();

	return 0;
}
