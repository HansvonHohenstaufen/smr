# smr - simple measure reactance

include config.mk

SRC = smr.c fft.c impedance.c serial.c util.c
OBJ = ${SRC:.c=.o}

all: options smr

options:
	@echo smr build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

smr: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f smr ${OBJ} smr-${VERSION}.tar.xz

dist: clean
	mkdir -p smr-${VERSION}
	cp -R ${SRC} config.h \
		arg.h fft.h impedance.h serial.h util.h \
		Makefile config.mk \
		smr.1 \
		LICENSE README \
		\smr-${VERSION}
	tar -cf smr-${VERSION}.tar smr-${VERSION}
	xz -9 -T0 smr-${VERSION}.tar
	rm -rf smr-${VERSION}

install: all
	mkdir -p ${PREFIX}/bin
	cp -f smr ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/smr
	mkdir -p ${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < smr.1 > ${MANPREFIX}/man1/smr.1

uninstall:
	rm -r ${PREFIX}/bin/smr \
		${MANPREFIX}/man1/logo.1

.PHONY: all options clean dist install uninstall
