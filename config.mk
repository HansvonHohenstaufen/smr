# smr version
VERSION = 0.1

# Paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# Includes and libs
LIBS =

# Flags
CPPFLAGS = -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\"
CFLAGS   = -std=c99 -Os -Wall ${CPPFLAGS}
LDFLAGS  = ${LIBS}

# Compiler
CC = cc
