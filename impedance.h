/* See LICENSE file for copyright and license details. */

#include "fft.h"

/* Functions declarations */
int impedance(int period, float *vi, float *vo, float *frequency, float *x);
