/* See LICENSE file for copyright and license details. */

/* Window size */
static const int window_parameters	= 3;
static const int window_measures	= 20;

/* Size from buffer data */
#define N	1024

/* Resistor */
static const float resistor = 2.2e3;
